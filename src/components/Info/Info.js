// @flow
/**
 * Discover velovity which was used to ride a given distance in a given time.
 * @param {props} distance in kilometers (Km)
 */
import React from 'react';
import './Info.css';

type Props = {
    airportToView: {
        name: string,
        city: string,
        country:string,
        iata: string
    };
}

export const Info = (props: Props) => {
    var airport = props.airportToView;
    if (Object.keys(airport).length > 0) {
        return (
            <div className="Info">
                <h1>{airport.name}</h1>
                <div className="Infodata">
                    <label>{airport.city} - </label>
                    <label>{airport.country} </label>
                    <label>({airport.iata})</label>
                </div>
            </div>
        );
    }
    else {
        return null;
    }
};