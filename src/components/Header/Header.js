// @flow

import React, {Component} from 'react';
import PropTypes from 'prop-types';

import './Header.css';

export default class Header extends Component {
    render() {
        const children = this.props.children;
        return (
            <div className="Header">
                <img
                    src={require('../../place-icon.png')}
                    className="Logo" alt="logo"
                />
                {children}
            </div>
        );
    }
}

Header.propTypes = {
    children: PropTypes.element.isRequired
};
