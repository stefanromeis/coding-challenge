// @flow

type State = {
    fetching: boolean,
    fetched: boolean,
    error: ?Object,
    mapInitialPos: Object,
    airportToView: Object,
    airportsByAutosuggest: Array<Object>,
    isLoading: boolean
}

const initialState = {
    fetching: false,
    fetched: false,
    error: null,
    mapInitialPos: {lat: 52.39886, lng: 13.06566},
    airportToView: {},
    airportsByAutosuggest: [],
    isLoading: false
};

const searchReducer = (state: State = initialState, action: Object): Object => {
    switch (action.type) {
        case "FETCH_AIRPORTS_PENDING": {
            return {
                ...state,
                fetching: true
            }
        }
        case "FETCH_AIRPORTS_REJECTED": {
            return {
                ...state,
                fetching: false,
                error: action.payload
            }
        }
        case "FETCH_AIRPORTS_FULFILLED": {
            return {
                ...state,
                fetching: false,
                fetched: true,
                airportsByAutosuggest: action.payload
            }
        }
        case "SET_AIRPORT": {
            return {
                ...state,
                airportToView: action.payload
            }
        }
        default:
            return state;
    }
};

export default searchReducer;